<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page import = "java.io.*,java.util.*,java.sql.*"%>
<%@ page import = "javax.servlet.http.*,javax.servlet.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix = "c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix = "sql"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="${pageContext.request.contextPath}/static/css/home.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/static/css/navbar.css" rel="stylesheet" type="text/css">
    <script src="${pageContext.request.contextPath}/static/javascript/script.js"></script>
<link href="${pageContext.request.contextPath}/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<script src="${pageContext.request.contextPath}/static/javascript/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />


  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script>
function setURL2(form) {
    var ab= $("#urlVal2").val();
    form.action+="stock/"+form.urlVal2.value;
    console.log(form.action);
    event.preventDefault();
    location.href =   form.action;  
}


$(document).ready(function(){
    $('[data-toggle="popover"]').popover();   
});

 </script>



</head>

<body>

<sql:setDataSource var = "snapshot" driver = "com.mysql.jdbc.Driver"
         url = "jdbc:mysql://localhost/Inzynierka"
         user = "root" password = "wrxpymww3cpf7"/>

 <sql:query dataSource = "${snapshot}" var = "result">
         SELECT * from stock;
      </sql:query>


 <div class="navbar" id="navbarTop">
  
     <sec:authorize access="hasRole('ROLE_USER')">
    <a href="${pageContext.request.contextPath}/dashboard">  <button type="button" class="btn btn-info">Kokpit</button> </a>   
   </sec:authorize>

  
  
  <ul class="list" >
 <li>
  <form action="${pageContext.request.contextPath}/" onsubmit="setURL2(this)">
  <select id="urlVal2" >
  <option value="volvo">Wybierz swoją spółkę</option>
  <c:forEach var = "row" items = "${result.rows}">
            <tr>
              <option><c:out value = "${row.name}"/></option>
            </tr>
         </c:forEach>
</select>
<input type="submit" class="btn btn-info" value="Znajdz Spółkę" />
</form>
  </li>
   <sec:authorize access="!hasRole('ROLE_USER')">
   <li> <a href="${pageContext.request.contextPath}/login">  <button type="button" class="btn btn-info">Zaloguj się</button> </a></li>    
   </sec:authorize>
    <li>
    <sec:authorize access="hasRole('ROLE_USER')">
    <c:url var="logoutUrl" value="/j_spring_security_logout"/>
     <form action="${logoutUrl}" method="post">
     <input type="submit" class="btn btn-warning" value="Wyloguj się"/>   
     <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
      </form>
 </sec:authorize>
 </li>
  
  
  </ul>
 <!-- Pojawienie sie loga tego smiesznego hamurgerka -->
 <a href="javascript:void(0);" class="icon" onclick="myFunction()">&#9776;     </a>  
 </div>
  

 
</body>
</html>