<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
  <meta name="_csrf"  content="${_csrf.token}"/>
  <meta name="_csrf_header" content="${_csrf.headerName}"/>

<script src="${pageContext.request.contextPath}/static/javascript/jquery-3.2.1.min.js"></script>
<script src="${pageContext.request.contextPath}/static/javascript/jquery.autocomplete.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>


</head>
<body>
	<h2>Spring MVC + jQuery + Autocomplete example</h2>

 <form>
 <div class="ui-widget">
 <label for="employee">Employee: </label>
 <input id="employee" />
 </div>
 </form>
 <div input name="_csrf" alt="qwe"/>	

<script>


$(document).ready(function() {
		  
//	  $.ajaxSetup
 //     (
    //     {
       //      cache:false,
     //        beforeSend: function (xhr) { xhr.setRequestHeader('Authorization', token); }
    //     }
    //  );
	
	
	 
	
	
	      var token =  $('meta[name="_csrf"]').attr("content");
		  var header = $("meta[name='_csrf_header']").attr("content");
		  alert(token);
		   $(document).ajaxSend(function(e, xhr, options) {
		    xhr.setRequestHeader('X-CSRF-Token',  token);
		   });
	
	

	$("#employee").autocomplete({
	source: function(request, response) {
	$.ajax({
	url: "${pageContext.request.contextPath}/autocomplete/getEmployees",
	type: "POST",
	data: { term: request.term },
	 
	dataType: "json",
	
	success: function(data) {
	response($.map(data, function(v,i){
	return {
	label: v.empName,
	value: v.empName
	};
	}));
	}
	});
	}
	});

	
	
	});


</script>






	
</body>
</html>