<%@ page language="java" contentType="text/html; UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 <%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; UTF-8">
<title>Insert title here</title>
<link href="${pageContext.request.contextPath}/static/css/Login.css" rel="stylesheet" type="text/css">



</head>
<body>
<div id="formularz">
<sf:form method="post" action="${pageContext.request.contextPath}/accountcreated" commandName="User">
<table class="formtable">
<tr><td class="label">    <input class="control"  path="username" name="username" type="text" placeholder="Login" onfocus="this.placeholder=''" onblur="this.placeholder='Login'" /> </input>   <div class="error">    <sf:errors path="username" >   </sf:errors>   </div>  </td></tr>
<tr><td class="Email">    <input class="control"  path="email"    name="email"    type="text" placeholder="Email" onfocus="this.placeholder=''" onblur="this.placeholder='Email'" /> </input>  <div class="error">    <sf:errors path="email">   </sf:errors>  </div>  </td></tr>
<tr><td class="Password"> <input class="control"  path="password" name="password" type="password" placeholder="Password" onfocus="this.placeholder=''" onblur="this.placeholder='Password'" rows="10" cols="10"></input>  <div class="error"> <sf:errors path="password">  </sf:errors>  </div>   </td></tr>
<tr><td class="Password"> <input class="control"  name="confimpass" type="password"   placeholder="Password" onfocus="this.placeholder=''" onblur="this.placeholder='Password'"             type="text" rows="10" cols="10"></input></td></tr>
<tr><td class="label">    <input class="control"  value="Zarejestruj się" type="submit" /></td></tr>
</table>
</sf:form>
</div>

</body>
</html>