<%@ page language="java" contentType="text/html; UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<html>
<meta http-equiv="Content-Type" content="text/html; UTF-8">
 <meta name="_csrf" content="${_csrf.token}"/>
  <meta name="_csrf_header" content="${_csrf.headerName}"/>
<head>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
<link href="${pageContext.request.contextPath}/static/css/home.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<script src="${pageContext.request.contextPath}/static/javascript/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />



<title>Insert title here</title>
<script>

function setURL(form) {
    form.action="stock/"+form.urlVal.value;
    console.log(form.action);
    event.preventDefault();
    location.href =   form.action;  
}
 

$(document).ready(function() {
	
    
	  var token =  $('meta[name="_csrf"]').attr("content");
	  var header = $("meta[name='_csrf_header']").attr("content");
	   $(document).ajaxSend(function(e, xhr, options) {
	    xhr.setRequestHeader('X-CSRF-Token',  token);
	   });



   $("#urlVal").autocomplete({
   source: function(request, response) {
   $.ajax({
   url: "${pageContext.request.contextPath}/test/action",
   type: "POST",
   data: { term: request.term },

   dataType: "json",

   success: function(data) {
   response($.map(data, function(v,i){
   return {
   label: v.name,
   value: v.name
};
}));
}
});
}
});
});



	
</script>
</head>

<body>

<jsp:include page="navbar.jsp"/>

<div class="main-theme">  <div class="black-box">  </div>

<div class="form-main" >
<form  action="" onsubmit="setURL(this)"   style="position:relative">
  
<input type="text" id="urlVal" class="form" placeholder="Wpisz nazwe spółki która cie interesuje" onfocus="this.placeholder=''" onblur="this.placeholder='Wpisz nazwe spółki która cie interesuje'" >  <button type="submit"> <i class="fa fa-search" aria-hidden="true"></i>  </button>  </input>

</form>
</div>
</div>

</body>
</html>