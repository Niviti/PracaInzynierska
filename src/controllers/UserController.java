package controllers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.text.DecimalFormat;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.taglibs.standard.lang.jstl.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import spring.web.dao.Indicators;
import spring.web.dao.Portfolio;
import spring.web.dao.Returns;
import spring.web.dao.Stock;
import spring.web.dao.User;
import spring.web.services.PortfolioService;
import spring.web.services.stockService;



@Controller
public class UserController {
	
	private PortfolioService portfolioservice;
	private stockService stockservice;
	private ResourceLoader resourceLoader;
	@Autowired
	public void setResourceLoader(ResourceLoader resourceLoader) 
	{
		this.resourceLoader = resourceLoader;
	}
	
	@Autowired
	public void setPortfolioService(PortfolioService portfolioservice)
	{
	    	this.portfolioservice = portfolioservice;
	}
	
	@Autowired
	public void setstockService(stockService stockservice)
	{
	    	this.stockservice = stockservice;
	}
	
	
	@RequestMapping("/dashboard")
	public ModelAndView showHome(HttpServletRequest request) throws java.text.ParseException,IOException
	{
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String username = auth.getName(); //get logged in username
		
		//Portfolio portfolio = new Portfolio();
		List<Portfolio> portfolio = portfolioservice.getPortfoilos(username);
	    
		ModelAndView map = new ModelAndView("dashboard");
		List<Stock> stocki = new ArrayList<Stock>();
		
		for(int i=0; i<portfolio.size(); i++)
		{
		 	Stock stock = stockservice.ReturnOneStockByid(portfolio.get(i).getIdstock());
		    stocki.add(stock); 
		}
		
		double StopyZwrotu=0.00;
		
		 // Wrzucamy nasz stock do map view
		for(int i=0; i<stocki.size(); i++)
		{
			
		      // Pobieranie sciezki
			  String path  = request.getSession().getServletContext().getRealPath("WebContent/resources/stock/"+stocki.get(i).getName()+".csv");
    	      // ta sciezka na serverze moze wygl�da� innaczej		
              String transform =path.replace(".metadata\\.plugins\\org.eclipse.wst.server.core\\tmp0\\wtpwebapps\\", "");
              Resource resource  = resourceLoader.getResource("file:"+transform);	
			
			  InputStream is = resource.getInputStream();
		      BufferedReader br = new BufferedReader(new InputStreamReader(is));
			  
		    
		      String line;
		      // to jest uzywane poniewaz w 1 linijce sa stringi
		      boolean test = false;
        	  List<String> daty = new ArrayList<String>();
        	  List<Double> notowania = new ArrayList<Double>();
		      while((line = br.readLine()) != null)
		      {
		      // Musi pomin�� 1 linnie bo tam jest zamkniecie;otwarcie;...
		      String[] parts = line.split(";");
		      if(test==true)
		      {
		      daty.add(parts[0]);
		      notowania.add(Double.parseDouble(parts[4])); 
		      }
		      test=true;
		      }
			 
		      // Kalendarz tworzymy 4 zwroty
		      Calendar cal = Calendar.getInstance();
		      cal.add(Calendar.MONTH, -1);
		
		      SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		
		      String date1 = format1.format(cal.getTime());
		      Date d1 = format1.parse(date1);
		      cal.add(Calendar.MONTH, +1);
		      cal.add(Calendar.MONTH, -3);
		      String date2 = format1.format(cal.getTime());
		      Date d2 = format1.parse(date2);
		      cal.add(Calendar.MONTH, +3);
		      cal.add(Calendar.MONTH, -6);
		      String date3 = format1.format(cal.getTime());
		      Date d3 = format1.parse(date3);
		      cal.add(Calendar.MONTH, +6);
		      cal.add(Calendar.MONTH, -12);
		      String date4 = format1.format(cal.getTime());
		      Date d4 = format1.parse(date4);
     
		      double close1=1; 
		      double close2=1;
		      double close3=1;
		      double close4=1;
		      
		      // Nasze Stopy Zwrotu M1 M3 M6 M12
		      for(int j=daty.size()-1;j>=0;j--)
		      {
		    	  Date nowa = format1.parse(daty.get(j));
		    	  if(close1==1 && (nowa.before(d1) || nowa.equals(d1)))
		    	  {
		    	  close1 = notowania.get(j);		    	
		    	  }
	        	  if(close2==1 && (nowa.before(d2) || nowa.equals(d2)))
			      {
			      close2 = notowania.get(j);
			      }
			      if(close3==1 && (nowa.before(d3) || nowa.equals(d3)))
			      {
			      close3 = notowania.get(j);
			      }
			      if(close4==1 && (nowa.before(d4) || nowa.equals(d4)))
			      {
			      close4 = notowania.get(j);
			      }  
		      }
		      System.out.print("Data");
		      System.out.print(daty.get(0));
		      // Wrzucamy Do Stringa stopy zwrotu z M1 M3 M6 M12 !!
		       Double c1 =  (((notowania.get(notowania.size()-1)/close1)-1)*100)+0.01;
		       Double c2 = (((notowania.get(notowania.size()-1)/close2)-1)*100)+0.01;
		       Double c3 = (((notowania.get(notowania.size()-1)/close3)-1)*100)+0.01;
		       Double c4 = (((notowania.get(notowania.size()-1)/close4)-1)*100)+0.01;
		       
		       DecimalFormat df2=new DecimalFormat("0.00");
		       String formate1 = df2.format(c1); 
		       String formate2 = df2.format(c2);
		       String formate3 = df2.format(c3);
		       String formate4 = df2.format(c4);
		       System.out.print("Start");
		       System.out.print(formate1);
		       System.out.print("Start");
		       System.out.print(formate2);
		       System.out.print("Start");
		       System.out.print(formate3);
		       System.out.print("Start");
		       System.out.print(formate4);
		       double finalValue1 = (Double)df2.parse(formate1) ;
		       double finalValue2 = (Double)df2.parse(formate2) ;
		       double finalValue3 = (Double)df2.parse(formate3) ;
		       double finalValue4 = (Double)df2.parse(formate4) ;
		       
		       Returns returns = new Returns(finalValue1,finalValue2,finalValue3,finalValue4);
		       stocki.get(i).setReturns(returns);
		       
		       
			      // Czas na Indicatory !!!
			      // RSI MACD MA ROC
			      
			      // EMA 21 14 
			      Double EMA21=0.00;
			      Double EMA14=0.00;
			      Double ROC21= 0.00;
			      Double licznikpetli=0.00;
			      Double licznik=0.00;
			      Double Mianownik=0.00;
			      Double  alpha= 1-(2.00/(21.00+1.00));
			      Double quad=0.00;
			      for(int j=notowania.size()-1;j>=0;j--)
			      {		
			    	licznik+=notowania.get(j)*Math.pow(alpha, licznikpetli);	        
			    	Mianownik+=Math.pow(alpha, licznikpetli);

			        if(licznikpetli==13)
			        {
			          EMA14=licznik/Mianownik;
			        }
			        
			        if(licznikpetli==20)
			        {
			          EMA21=licznik/Mianownik;
			          ROC21= ((notowania.get(notowania.size()-1)/notowania.get(j))-1)*100;
			          break;
			        }
			        licznikpetli++;
			      }
			      
			      // Deklaracja MACD
			      Double MACD = EMA14-EMA21;
			      
			      Indicators indicators = new Indicators();

			      
			      indicators.setMACD(MACD);
			      indicators.setROC(ROC21);
			      indicators.setROC(EMA21);
			      
			      
			      // Sygna�y Sprzeda�y i Kupna 
			      if(EMA21>=notowania.get(notowania.size()-1))
			      {
			    	  String EMAsynga� = "Kupuj";
			    	  indicators.setSignalEma(EMAsynga�);
			      }
			      else
			      {
			    	  String EMAsygna� = "Sprzedaj";
			    	  indicators.setSignalEma(EMAsygna�);
			      }
			      if(MACD>0)
			      {
			    	  String MACDsygna� = "Kupuj";
			    	  indicators.setSignalMACD(MACDsygna�);
			      }
			      else
			      {
			    	  String MACDsynga� = "Sprzedaj";
			    	  indicators.setSignalMACD(MACDsynga�);
			      }
			      if(ROC21>20)
			      {
			    	  String ROCsynga�="Sprzedaj";
			    	  indicators.setSignalROC(ROCsynga�);
			      }
			      if(ROC21<-20)
			      {
			    	  String ROCsynga�="Kupno";
			    	  indicators.setSignalROC(ROCsynga�);
			      }
			      if(ROC21>-20 && ROC21<20)
			      {
			    	  String ROCsynga�="Trzymaj";
			    	  indicators.setSignalROC(ROCsynga�);
			      }
			      
			      stocki.get(i).setIndicatots(indicators);
		       		     				
		
		
		

	      // Pobieranie sciezki
		  String path2  = request.getSession().getServletContext().getRealPath("WebContent/resources/stock/wig20.csv");
	      // ta sciezka na serverze moze wygl�da� innaczej		
        String transform2 =path2.replace(".metadata\\.plugins\\org.eclipse.wst.server.core\\tmp0\\wtpwebapps\\", "");
        Resource resource2  = resourceLoader.getResource("file:"+transform2);	
		
		  InputStream is2 = resource2.getInputStream();
	      BufferedReader br2 = new BufferedReader(new InputStreamReader(is2));
		  
	      //
	      String line2;
	      // to jest uzywane poniewaz w 1 linijce sa stringi
	      boolean test2 = false;
	      int licznikxxxx=0;
     	  List<Double> notowaniabety = new ArrayList<Double>();
	      while((line = br2.readLine()) != null)
	      {
	      // Musi pomin�� 1 linnie bo tam jest zamkniecie;otwarcie;...
	      String[] parts1 = line.split(";");
	      if(test2==true)
	      {
	      System.out.println(Double.parseDouble(parts1[4]));    
	      notowaniabety.add(Double.parseDouble(parts1[4])); 
	      System.out.println(notowaniabety.get(licznikxxxx));    
	      licznikxxxx++;
	      }
	      test2=true;
	      }
		
	      // teraz bierzemy notowania z 2 ostatnich lat
	      
	      Calendar cal2 = Calendar.getInstance();
	      cal2.add(Calendar.MONTH, -36);
	
	      SimpleDateFormat formatx = new SimpleDateFormat("yyyy-MM-dd");
	
	      String datex = formatx.format(cal2.getTime());
	      Date dx = formatx.parse(datex);
	      
	      int licznikdzielenia=0;
	      List<Double> wig20 = new ArrayList<Double>();
	      List<Double> stockx = new ArrayList<Double>();
	      Double Sumawig=0.00;
	      Double Sumastock=0.00;
	      int licznikxx=0;
	      for(int j=daty.size()-1;j>=0;j--)
	      {
	    	  Date nowa = formatx.parse(daty.get(j));
	    	  if(nowa.after(dx) ||  nowa.equals(dx))
	    	  {
	    		System.out.print("Data");
	    	    System.out.print(daty.get(0));
	    		wig20.add(notowaniabety.get(j));
	    		stockx.add(notowania.get(j));
	    		System.out.println(wig20.get(licznikxx));    
	    		System.out.println(stockx.get(licznikxx));   
	    		Sumawig+=wig20.get(licznikxx);
	    		Sumastock+=stockx.get(licznikxx);
	    		licznikdzielenia++;
	    		licznikxx++;
	    	  } 	  
	      }
	     Double sredniawig= Sumawig/licznikdzielenia;
		 Double sredniastock =Sumastock/licznikdzielenia;
		 System.out.print("srednie");
		 System.out.print(sredniawig);
		 System.out.print("/br");
		 System.out.print(sredniastock);
		//   System.out.println(sredniawig);
		//   System.out.println(sredniastock); 
		 
		 List<Double> stockminussrednia = new ArrayList<Double>();
		 List<Double> wigminussrednia = new ArrayList<Double>();
		 List<Double> mianownik2 = new ArrayList<Double>();
		 List<Double> licznik2 = new ArrayList<Double>();
		 Double SumaMianownik2=0.00;
		 Double SumaLicznik2=0.00;
		 int liczniktt=0;

		 for(int j=0;  j<stockx.size();j++)
		 {   
			 stockminussrednia.add(stockx.get(j)-sredniastock);
			 wigminussrednia.add(wig20.get(j)-sredniawig);
			 mianownik2.add( Math.pow(wigminussrednia.get(j),2));
			 licznik2.add(stockminussrednia.get(j)*wigminussrednia.get(j));
			 SumaMianownik2+=mianownik2.get(j);
			 SumaLicznik2+= licznik2.get(j); 	     
		 }
		
		  Double Beta = SumaLicznik2/SumaMianownik2;
		  

	    DecimalFormat df = new DecimalFormat("0.0000");      
	    Double beta2=  Double.parseDouble(df.format(Beta).replace(",", "."));

		  
		  stocki.get(i).setBeta(beta2);
		   
		 // wspolczynnik beta 
		
		
		
		/////////////////////
		// STOPA ZWROTU CA�EGO PORTFELA
		/////////////////////
		
		 Portfolio portfolio2= portfolioservice.ReturnOnePortfolioWhereIdandUsername(username, stocki.get(i).getIdstock());
		  
		  
		//Dzisiejsza stopa zwrotu  
		double first = notowania.get(notowania.size()-1);  
		double maxstopa=0.00;
		for(int j=notowania.size()-1; j>=0;j--)
		{
			
			Date nowa = format1.parse(daty.get(j));
			if(maxstopa==0.00 & nowa.before(portfolio2.getDate()))
			{
		    maxstopa = notowania.get(j);	
		    }
			
		}

		double StopaZwrotu = portfolio2.getBalance()*((first/maxstopa)-1);
		StopyZwrotu+=StopaZwrotu;
		
		}
		
		double sumabety=0;
		
		for(int i=0; i<stocki.size();i++)
		{
			for(int j=0; j<portfolio.size(); j++)
			{
				if(stocki.get(i).getIdstock()== portfolio.get(j).getIdstock())			
				{
					sumabety+=stocki.get(i).getBeta()*portfolio.get(j).getBalance();
				}
			}
		}
		
		DecimalFormat df = new DecimalFormat("#.###");     
		
		String Sumabet2 = df.format(sumabety);
		
		// sumabety 
		map.addObject("sumabety", Sumabet2.replace(',', '.'));
		StopyZwrotu=StopyZwrotu*100;
		
	    
		String StopyZwrotu2 = df.format(StopyZwrotu);
		
		map.addObject("StopyZwrotu", StopyZwrotu2.replace(',', '.'));
		
		map.addObject("stocks", stocki);
		
		map.addObject("portfolios", portfolio);
		 
	    return map;
	}
	
	@RequestMapping("/dashboard/portfolio")
	public String AddPortfolio()
	{
		return "portfolio";
	}
	
	
	//@ExceptionHandler(DataAccessException.class)
	//public String handleDatabaseException(DataAccessException ex) {
	//	return "error";
	//}	
	@RequestMapping(value="/dashboard/portfolio/create",  method=RequestMethod.POST)
	public String CreatePortfolio(HttpServletRequest request) throws java.text.ParseException
	{		
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String username = auth.getName(); //get logged in username
		
		// usuwamy wszystko od usera na poczatku
		List<Portfolio> portfolio = portfolioservice.getPortfoilos(username);
		
		 for(int i=0; i<portfolio.size(); i++)
		  {
			portfolioservice.delete(portfolio.get(i).getIdportfolio());
    		}
		
		
		   List<String> akcje = new ArrayList<String>();
		 //  List<Double> wagi = new ArrayList<Double>();
		   int licznik=0;
	    	Map<String,String[]> allMap= request.getParameterMap();
	         for(String key:allMap.keySet()){
	        String[] strArr=(String[])allMap.get(key);
	        for(String val:strArr){	       
	        	 akcje.add(val);
	        	 System.out.println(val);

	      }   
	    }
	         
	    System.out.println("teraz dobre");
	    
	    List<String> akcje2 = new ArrayList<String>();
	 
	    List<Double> wagi = new ArrayList<Double>();
	    
	    List<Date> daty = new ArrayList<Date>();
	    
	   // akcje.remove(akcje.size()-1);
	    
	    akcje.remove(0);
	    boolean testowa=false;
	    SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
	 //   licznik = akcje.size()/2;
	    licznik=0;
	    int licznikfalse=1;
	    for(int i=0; i<akcje.size(); i++)
	    {    
	    	if(licznik==0)
	    	     {
	    		   akcje2.add(akcje.get(i));
	    	     }	
                 if((licznik%2) ==0 && (licznik%3)!=0 && testowa==false)
                 {
                	  wagi.add(Double.parseDouble(akcje.get(i)));
                 }
                 if((licznik%2) ==0 && (licznik%3)!=0 && testowa==true)
                 {
                	 Date d1 = format1.parse(akcje.get(i));
                	 daty.add(d1);
                 }
                 if((licznik%2) ==1 && (licznik%3)!=0 && testowa==false)
                 {
	         	     wagi.add(Double.parseDouble(akcje.get(i)));
                 }	  	
                 if((licznik%2) ==1 && (licznik%3)!=0 && testowa==true)
                 {
                	 Date d1 = format1.parse(akcje.get(i));
                	 daty.add(d1);
                 }
                 if((licznik%3)==0 && licznik!=0)
                 {
                	 akcje2.add(akcje.get(i));
                 }      
                 
                 if(testowa==true && (licznik%3)!=0)
                 {
                	testowa=false; 
                	;
                 }
                 else if(testowa==false && (licznik%3)!=0)
                 {
                	 testowa=true;
                 }
                 licznik++;
                 
	        System.out.println(akcje.get(i));
	     
	    }
	    

	 
		
	    //  List<Integer> nowy = new ArrayList<Integer>();
	    
	      List<Integer> nowy=stockservice.ReturnAllStocksID(akcje2);
	    
	//    portfolioservice.addPortfolio();

 
	    for(int i=0; i< akcje2.size(); i++)
	    {
	    	// try
	  	//    {
	    	Portfolio portfolio2 = new Portfolio();
	    	portfolio2.setUsername(username);
	    	portfolio2.setBalance(wagi.get(i));
	    	portfolio2.setIdstock(nowy.get(i));
	    	portfolio2.setName(akcje2.get(i));
            portfolio2.setDate(daty.get(i));
	    	portfolioservice.addPortfolio(portfolio2);
	  //      }
	      //  catch(Exception e)
	 	  //  {
	 	    	//System.out.println(e.getMessage());
	 	    //	return "portfolio";
	 	 //   }
	    }
	    	
		return "dashboard";
	}
	
	@RequestMapping(value="/dashboard/portfolio/update",  method=RequestMethod.GET)
	public ModelAndView UpdatePortfolio()
	{		
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String username = auth.getName(); //get logged in username
		
		//Portfolio portfolio = new Portfolio();
		List<Portfolio> portfolio = portfolioservice.getPortfoilos(username);
	    
		ModelAndView map = new ModelAndView("portfolio-update");
		
		map.addObject("portfolios", portfolio);
		
	
		return  map;
	}
	
	@RequestMapping(value="/dashboard/portfolio/update/action",  method=RequestMethod.POST)
	public String UpdateActionPortfolio(HttpServletRequest request) throws java.text.ParseException
	{		
		   		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String username = auth.getName(); //get logged in username
		
		// usuwamy wszystko od usera na poczatku
		List<Portfolio> portfolio = portfolioservice.getPortfoilos(username);
		
		 for(int i=0; i<portfolio.size(); i++)
		  {
			portfolioservice.delete(portfolio.get(i).getIdportfolio());
    		}
				
		   List<String> akcje = new ArrayList<String>();
			 //  List<Double> wagi = new ArrayList<Double>();
			   int licznik=0;
		    	Map<String,String[]> allMap= request.getParameterMap();
		         for(String key:allMap.keySet()){
		        String[] strArr=(String[])allMap.get(key);
		        for(String val:strArr){	       
		        	 akcje.add(val);
		      }   
		    }
		         
		         
		 	    List<String> akcje2 = new ArrayList<String>();
		 	 
		 	    List<Double> wagi = new ArrayList<Double>();
		 	    
		 	   List<Date> daty = new ArrayList<Date>();
		 	    
		 	   // akcje.remove(akcje.size()-1);
		 	    
		 	    akcje.remove(0);
		 	    boolean testowa=false;
		 	 //   licznik = akcje.size()/2;
		 	    licznik=0;
		 	   SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		 	   for(int i=0; i<akcje.size(); i++)
			    {    
			    	if(licznik==0)
			    	     {
			    		   akcje2.add(akcje.get(i));
			    	     }	
		                 if((licznik%2) ==0 && (licznik%3)!=0 && testowa==false)
		                 {
		                	  wagi.add(Double.parseDouble(akcje.get(i)));
		                 }
		                 if((licznik%2) ==0 && (licznik%3)!=0 && testowa==true)
		                 {
		                	 Date d1 = format1.parse(akcje.get(i));
		                	 daty.add(d1);
		                 }
		                 if((licznik%2) ==1 && (licznik%3)!=0 && testowa==false)
		                 {
			         	     wagi.add(Double.parseDouble(akcje.get(i)));
		                 }	  	
		                 if((licznik%2) ==1 && (licznik%3)!=0 && testowa==true)
		                 {
		                	 Date d1 = format1.parse(akcje.get(i));
		                	 daty.add(d1);
		                 }
		                 if((licznik%3)==0 && licznik!=0)
		                 {
		                	 akcje2.add(akcje.get(i));
		                 }      
		                 
		                 if(testowa==true && (licznik%3)!=0)
		                 {
		                	testowa=false; 
		                	;
		                 }
		                 else if(testowa==false && (licznik%3)!=0)
		                 {
		                	 testowa=true;
		                 }
		                 licznik++;
		                 
			        System.out.println(akcje.get(i));
			     
			    }
		 	    
		 	    
		 	    
		 	  
		 	 List<Integer> nowy=stockservice.ReturnAllStocksID(akcje2);
		         
		 	  for(int i=0; i< akcje2.size(); i++)
			    {
			    	 try
			  	    {
			    	Portfolio portfolio2 = new Portfolio();
			    	portfolio2.setUsername(username);
			    	portfolio2.setBalance(wagi.get(i));
			    	portfolio2.setIdstock(nowy.get(i));
			    	portfolio2.setName(akcje2.get(i));
			    	portfolio2.setDate(daty.get(i));
			    	portfolioservice.addPortfolio(portfolio2);
			        }
			        catch(Exception e)
			 	    {
			 	    	System.out.println(e.getMessage());
			 	    	return "portfolio-update";
			 	    }
			    }
		         		    						
		  return  "dashboard";
				
	}
	
	
}
