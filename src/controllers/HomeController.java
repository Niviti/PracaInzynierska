package controllers;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

//import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import spring.web.dao.Stock;
import spring.web.services.ReadFileService;
import spring.web.services.usersService;
import spring.web.services.stockService;

@Controller
public class HomeController {

	private ReadFileService readFileService;
	private ResourceLoader resourceLoader;
	private ClassPathResource pathResource;
	ServletContext servletContext;
	private stockService stockService;
	
	@Autowired
	public void setstockService(stockService stockService) 
	{
		this.stockService = stockService;
	}
	
	
	@Autowired
	public void setResourceLoader(ResourceLoader resourceLoader) 
	{
		this.resourceLoader = resourceLoader;
	}
	
	@Autowired
	public void setReadFileService(ReadFileService readFileService) 
	{
		this.readFileService = readFileService;
	}
	
	@Autowired
	public void setServletContext(ServletContext servletContext) 
	{
		this.servletContext = servletContext;
	}
	
	@RequestMapping(value ="/test/action", method = { RequestMethod.GET, RequestMethod.POST  })
	public @ResponseBody List<Stock> AjaxText(@RequestParam("term") String stockName)
	{
		
		System.out.println(stockName);
		
		List<Double> notowania = new ArrayList<Double>();
		
		List<Stock> stocki = new ArrayList<Stock>();
		
	    stocki = stockService.ReturnAllStocks();

	 	System.out.println(stocki.get(0).getName());
	    
	    List<Stock> result = new ArrayList<Stock>();
	    
		// iterate a list and filter by tagName
		for (Stock stock : stocki) {
			if ( stock.getName().contains(stockName)) {
			     result.add(stock);
			}
		}
	    
		//System.out.println(result.get(0));
	    return result;
	}
	
	@RequestMapping(value ="/test",method = RequestMethod.GET)
	public ModelAndView ShowTest()
	{
		
		ModelAndView map = new ModelAndView("Ajaxtest");
	    return map;
	}
	  
	@RequestMapping(value="/", method = RequestMethod.GET)
	public String showHome()
	{
	    return "home";
	}
   
	@RequestMapping("/chart")
	public String showChart()
	{
		  return "chart";
	}
	
	@RequestMapping("/chart2")
	public String showChart2()
	{
		  return "chart3";
	}
	
	
	@RequestMapping(value="/stock/{stockname}", method = RequestMethod.GET)
	//@ResponseBody
	public ModelAndView showStock(@PathVariable String stockname, HttpServletRequest request) throws IOException, ParseException
	{ 			   	   				  
		      ModelAndView map = new ModelAndView("stock");
		  		     
		      System.out.println(stockname);
		    
		      Stock stock = stockService.ReturnOneStockByName(stockname);
		    
		      // Wrzucamy nasz stock do map view
		      
		      map.addObject("stock", stock);
		      
	     	  String path  = request.getSession().getServletContext().getRealPath("WebContent/resources/stock/"+stock.getName()+".csv");
    	      // ta sciezka na serverze moze wygl�da� innaczej		
              String transform =path.replace(".metadata\\.plugins\\org.eclipse.wst.server.core\\tmp0\\wtpwebapps\\", "");
        		
        	  Resource resource  = resourceLoader.getResource("file:"+transform);	 
		     
        	  InputStream is = resource.getInputStream();
		      BufferedReader br = new BufferedReader(new InputStreamReader(is));
		    		         
		      String line;
		      boolean test = false;
        	  List<String> daty = new ArrayList<String>();
        	  List<Double> notowania = new ArrayList<Double>();
		      while((line = br.readLine()) != null)
		      {
		      // Musi pomin�� 1 linnie bo tam jest zamkniecie;otwarcie;...
		      String[] parts = line.split(";");
		      if(test==true)
		      {
		      daty.add(parts[0]);
		      notowania.add(Double.parseDouble(parts[4])); 
		      }
		      test=true;
		      }
		      // deklaracja daty aby zdobyc 4 daty - 1 -3 -6 -12... 
		  
		      Calendar cal = Calendar.getInstance();
		      cal.add(Calendar.MONTH, -1);
		
		      SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		
		      String date1 = format1.format(cal.getTime());
		      Date d1 = format1.parse(date1);
		      cal.add(Calendar.MONTH, +1);
		      cal.add(Calendar.MONTH, -3);
		      String date2 = format1.format(cal.getTime());
		      Date d2 = format1.parse(date2);
		      cal.add(Calendar.MONTH, +3);
		      cal.add(Calendar.MONTH, -6);
		      String date3 = format1.format(cal.getTime());
		      Date d3 = format1.parse(date3);
		      cal.add(Calendar.MONTH, +6);
		      cal.add(Calendar.MONTH, -12);
		      String date4 = format1.format(cal.getTime());
		      Date d4 = format1.parse(date4);
          
		      double close1=1; 
		      double close2=1;
		      double close3=1;
		      double close4=1;
		      // Nasze Stopy Zwrotu M1 M3 M6 M12
		      for(int i=daty.size()-1;i>=0;i--)
		      {
		    	  Date nowa = format1.parse(daty.get(i));
		    	  if(close1==1 && (nowa.before(d1) || nowa.equals(d1)))
		    	  {
		    	  close1 = notowania.get(i);		    	
		    	  }
	        	  if(close2==1 && (nowa.before(d2) || nowa.equals(d2)))
			      {
			      close2 = notowania.get(i);
			      }
			      if(close3==1 && (nowa.before(d3) || nowa.equals(d3)))
			      {
			      close3 = notowania.get(i);
			      }
			      if(close4==1 && (nowa.before(d4) || nowa.equals(d4)))
			      {
			      close4 = notowania.get(i);
			      }  
		      }
		      
		       System.out.println(notowania.get(notowania.size()-1));
		       System.out.println(close1);
		       System.out.println(close2);
		       System.out.println(close3);
		       System.out.println(close4);
		      
		      // Wrzucamy Do Stringa stopy zwrotu z M1 M3 M6 M12 !!
		       // Wrzucamy Do Stringa stopy zwrotu z M1 M3 M6 M12 !!
		       Double c1 =  (((notowania.get(notowania.size()-1)/close1)-1)*100)+0.01;
		       Double c2 = (((notowania.get(notowania.size()-1)/close2)-1)*100)+0.01;
		       Double c3 = (((notowania.get(notowania.size()-1)/close3)-1)*100)+0.01;
		       Double c4 = (((notowania.get(notowania.size()-1)/close4)-1)*100)+0.01;
		      
		       
		       DecimalFormat df2=new DecimalFormat("0.00");
		       String formate1 = df2.format(c1); 
		       String formate2 = df2.format(c2);
		       String formate3 = df2.format(c3);
		       String formate4 = df2.format(c4);
		       double finalValue1 = (Double)df2.parse(formate1) ;
		       double finalValue2 = (Double)df2.parse(formate2) ;
		       double finalValue3 = (Double)df2.parse(formate3) ;
		       double finalValue4 = (Double)df2.parse(formate4) ;
		       
		       
		 	  map.addObject("c1", finalValue1);
			  map.addObject("c2", finalValue2);
			  map.addObject("c3", finalValue3);
			  map.addObject("c4", finalValue4);
		      
		      // Czas na Indicatory !!!
		      // RSI MACD MA ROC
		      
		      // EMA 21 14 
		      Double EMA21=0.00;
		      Double EMA14=0.00;
		      Double ROC21= 0.00;
		      Double licznikpetli=0.00;
		      Double licznik=0.00;
		      Double Mianownik=0.00;
		      Double  alpha= 1-(2.00/(21.00+1.00));
		      Double quad=0.00;
		      for(int i=notowania.size()-1;i>=0;i--)
		      {		
		    	licznik+=notowania.get(i)*Math.pow(alpha, licznikpetli);	        
		    	Mianownik+=Math.pow(alpha, licznikpetli);

		        if(licznikpetli==13)
		        {
		          EMA14=licznik/Mianownik;
		        }
		        
		        if(licznikpetli==20)
		        {
		          EMA21=licznik/Mianownik;
		          ROC21= ((notowania.get(notowania.size()-1)/notowania.get(i))-1)*100;
		          break;
		        }
		        licznikpetli++;
		      }
		      
		      // Deklaracja MACD
		      Double MACD = EMA14-EMA21;
		 
		      // Sygna�y Sprzeda�y i Kupna 
		      if(EMA21>=notowania.get(notowania.size()-1))
		      {
		    	  String EMAsynga� = "Kupuj";
		    	  map.addObject("EMAsygna�",EMAsynga�);
		      }
		      else
		      {
		    	  String EMAsygna� = "Sprzedaj";
		    	  map.addObject("EMAsygna�", EMAsygna�);
		      }
		      if(MACD>0)
		      {
		    	  String MACDsygna� = "Kupuj";
		    	  map.addObject("MACDsynga�", MACDsygna�);
		      }
		      else
		      {
		    	  String MACDsynga� = "Sprzedaj";
		    	  map.addObject("MACDsynga�", MACDsynga�);
		      }
		      if(ROC21>20)
		      {
		    	  String ROCsynga�="Sprzedaj";
		    	  map.addObject("ROC21", ROCsynga�);
		      }
		      if(ROC21<-20)
		      {
		    	  String ROCsynga�="Kupno";
		    	  map.addObject("ROC21", ROCsynga�);
		      }
		      if(ROC21>-20 && ROC21<20)
		      {
		    	  String ROCsynga�="Trzymaj";
		    	  map.addObject("ROC21", ROCsynga�);
		      }
		   
		      
	          return map; 
    }
}
