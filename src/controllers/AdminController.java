package controllers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import spring.web.dao.Stock;
import spring.web.services.CSVUtils;
import spring.web.services.ReadFileService;
import spring.web.services.usersService;
import spring.web.services.stockService;

import org.springframework.core.io.ResourceLoader;


@Controller
public class AdminController {
    
	private CSVUtils csvutils;
    private ServletContext servletContext;
    private stockService stockService;
    private ResourceLoader resourceLoader;
    
    @Autowired
    public void setResourceLoader(ResourceLoader resourceLoader)
    {
    	this.resourceLoader = resourceLoader;
    }
    
    @Autowired
    public void setstockService(stockService stockService)
    {
    	this.stockService= stockService;
    }
    
	@Autowired
	public void setCSVUtils(CSVUtils CSVUtils)
	{
		this.csvutils = CSVUtils; 
	}
	
	@Autowired
	public void setServletContext(ServletContext servletContext)
	{
	 this.servletContext = servletContext;	   
	}
	
	@RequestMapping(value="/admin/panel", method= RequestMethod.GET)
	public ModelAndView AdminPanel(HttpServletRequest request) throws IOException
	{
	    
		List<Stock> stocki = new ArrayList<Stock>();
		
	    stocki =stockService.ReturnAllStocks();
	    	
		ModelAndView map = new ModelAndView("AdminPanel");
 
		map.addObject("stocks", stocki);
		
		return map;
	}
	
	@RequestMapping(value="/admin/panel/stockcreat", method= RequestMethod.GET)
	public ModelAndView AdminPanelCreate(HttpServletRequest request) throws IOException
	{
		
		ModelAndView map = new ModelAndView("AdminPanelCreate");
		
		return map;
	}
	
	@RequestMapping(value="/admin/panel/stockcreat/action", method= RequestMethod.POST)
	public String AdminPanelCreateAction(Model model, @Valid Stock stock, BindingResult result) 
	{
		
		if(result.hasErrors()) {
			return "AdminPanelCreate";
		}
		
		stockService.CreatStock(stock);
		
		return "AdminPanel";
	}
	

	
	@RequestMapping(value="/admin/panel/stockupdate/{param}", method= {RequestMethod.POST,  RequestMethod.GET   })
	public ModelAndView AdminPanelUpdateAction(@PathVariable int param) 
	{
		ModelAndView map = new ModelAndView("AdminPanelUpdate");
		
	    Stock stock = stockService.ReturnOneStockByid(param);
		
	    map.addObject("stock", stock);
	    
		return map;
	}
	
	@RequestMapping(value="/admin/panel/stockcreat/update/action", method= RequestMethod.POST)
	public String AdminPanelUpdateAction(Model model, @Valid Stock stock, BindingResult result) 
	{
		
	  System.out.println(stock.getName());
	  System.out.println(stock.getIdstock());
		
		stockService.UpdateStock(stock);
		//stockService.CreatStock(stock);
		
		return "AdminPanel";
	}
	
	
	
	
	
	@RequestMapping(value="/admin/getstock", method= RequestMethod.GET)
	public ModelAndView GetStock(HttpServletRequest request) throws IOException
	{
	    ModelAndView map = new ModelAndView("getstock");
        // Zwraca katalog w ktorym znajduje sie caly projekt
        String path  = request.getSession().getServletContext().getRealPath("WebContent/resources/stock/stock.csv");
        // ta sciezka na serverze moze wygl�da� innaczej	
		String transform =path.replace(".metadata\\.plugins\\org.eclipse.wst.server.core\\tmp0\\wtpwebapps\\", "");
        FileWriter writer = new FileWriter(transform);
    	
	    csvutils.writeLine(writer, Arrays.asList("Name;", "Data;", "Otwarcie;", "Najwyzszy;", "Najnizszy;", "Zamkniecie;","Wolumen"), true );
		
	    try {
		URL url = new URL("http://bossa.pl/pub/metastock/mstock/sesjaall/sesjaall.prn");
			        URLConnection urlConn = url.openConnection();
			        BufferedReader in = new BufferedReader(
			                                new InputStreamReader(
			                                urlConn.getInputStream()));
		    String inputLine;
		    String Stock;
			List<String> notowania = new ArrayList<String>();
			while ((inputLine = in.readLine()) != null) 
			{
		    Stock = inputLine.replace("," , ";");
		    notowania.add(Stock);;
			}
			map.addObject("notowania", notowania); 
	        csvutils.writeLine(writer, notowania, false);
			in.close();
		} catch (Exception e) {
		  System.out.println(e.getMessage());
		}
		
		return map;
	}
	
	@RequestMapping(value="/admin/updatestock", method= RequestMethod.GET)
	public ModelAndView UpdateStock(HttpServletRequest request) throws IOException
	{
		  ModelAndView map = new ModelAndView("updatestock");
		
		  String path  = request.getSession().getServletContext().getRealPath("WebContent/resources/stock/stock.csv");
		  // ta sciezka na serverze moze wygl�da� innaczej	
		  String transform =path.replace(".metadata\\.plugins\\org.eclipse.wst.server.core\\tmp0\\wtpwebapps\\", "");
		
		  Resource resource  = resourceLoader.getResource("file:"+transform);	 

          List<Stock> nowa = new ArrayList<Stock>();
          nowa = stockService.ReturnAllStocks();
          String line;
          for(int i=0; i<nowa.size(); i++)
          {
        	
        	  InputStream is = resource.getInputStream();
        	  BufferedReader br = new BufferedReader(new InputStreamReader(is));
              while ((line = br.readLine()) != null) {		    		        	  
              String tekst = line;
              int liczba1 =tekst.indexOf(';')-1;
              String nowy = nowa.get(i).getName()+";";
              //jesli tekst pazuje i liczba liter tez wtedy bierzemy ten kawe�ek i wpsiujemy do pliku ORLNPZU zwraca jako pzu
              int liczba2 = nowy.indexOf(';');
              if(tekst.contains(nowa.get(i).getName()+";") && liczba1==liczba2)
              {        	 
            	  String path2  = request.getSession().getServletContext().getRealPath("WebContent/resources/stock/"+nowa.get(i).getName()+".csv");
        	      // ta sciezka na serverze moze wygl�da� innaczej		
                  String transform2 =path2.replace(".metadata\\.plugins\\org.eclipse.wst.server.core\\tmp0\\wtpwebapps\\", "");
            		
            	  Resource resource2  = resourceLoader.getResource("file:"+transform2);	 
            	 
            	  InputStream is2 = resource2.getInputStream();
  		          BufferedReader br2 = new BufferedReader(new InputStreamReader(is2));
  		    		         
  		          String line2;
  		          List<String> notowania = new ArrayList<String>();
  		          while((line2 = br2.readLine()) != null)
  		          {
  		        	  notowania.add(line2);
  		          }
  		          String Stock = tekst.replace(nowa.get(i).getName()+";","");
  		          
  		          // zmiany daty z 27092017 na 27-09-2017
  		          int pomocna =Stock.indexOf(';');
  		          
  		          String data1 = Stock.substring(1,5);
  		          String data2 = Stock.substring(5,7);
  		          String data3 = Stock.substring(7,9);
  		          
  		          String Magic = Stock.substring(0,9);
  		          
  		          String datafinall = data1+"-"+data2+"-"+data3;
  		          
  		          System.out.println(datafinall);
  		          
  		          String Crash = Stock.replace(Magic, datafinall);
  		          
  		          System.out.println(Crash);
  		          
  		          notowania.add(Crash);
  		  	           
		          map.addObject("notowania", notowania); 
		         
		 
 		 	     FileWriter writer2 = new FileWriter(transform2);
 		 	     BufferedWriter out = new BufferedWriter(writer2);
 		 	     for(String s : notowania)
 		 	     {
 	                 out.write(s);
 	                 out.write("\n");
 		 	     }
 	            out.flush();
 	            out.close();
 		 	    br2.close();    
             }
          }         
          }
       
		return map;
	}
}
