package spring.web.services;

import  spring.web.dao.StockDao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import  spring.web.dao.Stock;


@Service("stockService")
public class stockService {

	StockDao stockdao;
	
    @Autowired
	public void setStockDao(StockDao stockdao)
	{
		this.stockdao = stockdao;
	}
	
	public List<Stock> ReturnAllStocks()
	{   
		return stockdao.ReturnAll();
	}
	
	public List<Integer> ReturnAllStocksID(List<String> stocki)
	{
		return stockdao.ReturnAllByName(stocki);
	}
	
	public Stock ReturnOneStockByName(String name)
	{
		return stockdao.ReturnOneStockByName(name);
	}
	
	public Stock ReturnOneStockByid(int id)
	{
	 return stockdao.ReturnsOneStockById(id);	
	}
	
	public boolean CreatStock(Stock stock)
	{
		return stockdao.create(stock);
	}
	
	public boolean UpdateStock(Stock stock)
	{
		return stockdao.update(stock);
	}
	
	
	
}
