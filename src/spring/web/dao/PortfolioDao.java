package spring.web.dao;

import javax.sql.DataSource;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.RowMapper;





@Component("PortfolioDao")
public class PortfolioDao {

    private NamedParameterJdbcTemplate jdbc;
	
	@Autowired
	public void setDataSource(DataSource jdbc) {
		this.jdbc = new NamedParameterJdbcTemplate(jdbc);
	}
	
	public PortfolioDao()
    {  
		System.out.println("Jest Power Dzia�a !!!");
    }
	
	public void create(Portfolio portfolio)
	{
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(portfolio);
		// dziala tak na prawde nie pacz na to !!!
	  	jdbc.update("insert into portfolio (idportfolio, idstock, balance, username,name,date) values (:idportfolio, :idstock, :balance, :username, :name, :date)", params);	
	}
	
	public List<Portfolio> ReturnAllPortfolioByUserName(String username)
	{
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("username", username);
		
		List<Portfolio> nowa = new ArrayList<Portfolio>();
		return jdbc.query("select * from portfolio where username=:username",params,	
				new RowMapper<Portfolio>() {

			      public Portfolio mapRow(ResultSet rs, int rowNum)
					     throws SQLException {

                         Portfolio portfolio = new Portfolio();
                         
                         portfolio.setIdstock(rs.getInt("idstock"));
                         portfolio.setBalance(rs.getDouble("balance"));
                         portfolio.setIdportfolio(rs.getInt("idportfolio"));
                         portfolio.setName(rs.getString("name"));
                         portfolio.setUsername(rs.getString("username"));
                         portfolio.setDate(rs.getDate("date"));   
			             return portfolio;
		          	}
		      });	
	}
	
	public Portfolio ReturnOnePortfolioWhereIdandUsername(String name, int idstock)
	{
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("username", name);
		params.addValue("idstock", idstock);
		
		return jdbc.queryForObject("select * from portfolio where idstock=:idstock AND username=:username ", params,
				new RowMapper<Portfolio>() {

			public Portfolio mapRow(ResultSet rs, int rowNum)
					throws SQLException {

                Portfolio portfolio = new Portfolio();
                portfolio.setIdstock(rs.getInt("idstock"));
                portfolio.setBalance(rs.getDouble("balance"));
                portfolio.setIdportfolio(rs.getInt("idportfolio"));
                portfolio.setName(rs.getString("name"));
                portfolio.setUsername(rs.getString("username"));
                portfolio.setDate(rs.getDate("date"));   
				return portfolio;
			}
		});	
	}
	
	
	public boolean delete(int id)
	{
		 MapSqlParameterSource params = new MapSqlParameterSource("idportfolio", id);
		 
		 return jdbc.update("delete from portfolio where idportfolio=:idportfolio", params)==1;
	}
	
	public boolean deleteByStockId(int id)
	{
		 MapSqlParameterSource params = new MapSqlParameterSource("idstock", id);
		 
		 return jdbc.update("delete from portfolio where idstock=:idstock", params)==1;
	}
	
}
