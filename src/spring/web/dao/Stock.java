package spring.web.dao;

public class Stock {

	private int idstock;
	private String name;
	private double PE;
	private double PS;
	private double PB; 
	public double getBeta() {
		return beta;
	}
	public void setBeta(double beta) {
		this.beta = beta;
	}

	private double ROE;
	public Stock(int idstock, String name, double pE, double pS, double pB, double rOE, double rOA, double beta,
			Indicators indicatots, Returns returns) {
		super();
		this.idstock = idstock;
		this.name = name;
		PE = pE;
		PS = pS;
		PB = pB;
		ROE = rOE;
		ROA = rOA;
		this.beta = beta;
		this.indicatots = indicatots;
		this.returns = returns;
	}

	private double ROA;  
	private double beta;
	private Indicators indicatots;
	private Returns returns;
	
	public int getIdstock() {
		return idstock;
	}
	public void setIdstock(int idstock) {
		this.idstock = idstock;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPE() {
		return PE;
	}
	public void setPE(double pE) {
		PE = pE;
	}
	public double getPS() {
		return PS;
	}
	public void setPS(double pS) {
		PS = pS;
	}
	public double getPB() {
		return PB;
	}
	public void setPB(double pB) {
		PB = pB;
	}
	public double getROE() {
		return ROE;
	}
	public void setROE(double rOE) {
		ROE = rOE;
	}
	public double getROA() {
		return ROA;
	}
	public void setROA(double rOA) {
		ROA = rOA;
	} 
	
	
	public Indicators getIndicatots() {
		return indicatots;
	}
	public void setIndicatots(Indicators indicatots) {
		this.indicatots = indicatots;
	}
	public Returns getReturns() {
		return returns;
	}
	public void setReturns(Returns returns) {
		this.returns = returns;
	}
	
	
	public Stock(int idstock, String name, double pE, double pS, double pB, double rOE, double rOA,
			Indicators indicatots, Returns returns) {
		super();
		this.idstock = idstock;
		this.name = name;
		PE = pE;
		PS = pS;
		PB = pB;
		ROE = rOE;
		ROA = rOA;
		this.indicatots = indicatots;
		this.returns = returns;
	}
	
	public Stock(int idstock, String name, double PE, double PS, double PB, double ROE, double ROA)
	{
		this.idstock= idstock;
		this.name= name;
		this.PE = PE;
		this.PS = PS;
		this.PB = PB;
		this.ROE = ROE;
		this.ROA =ROA;
	}
	
	public Stock()
	{
		
	} 
	
}
