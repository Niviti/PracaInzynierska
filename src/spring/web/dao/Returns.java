package spring.web.dao;

public class Returns {

	private double M1;
	private double M3;
	private double M6;
	private double M12;
	
	public double getM1() {
		return M1;
	}
	public void setM1(double m1) {
		M1 = m1;
	}
	public double getM3() {
		return M3;
	}
	public void setM3(double m3) {
		M3 = m3;
	}
	public double getM6() {
		return M6;
	}
	public void setM6(double m6) {
		M6 = m6;
	}
	public double getM12() {
		return M12;
	}
	public void setM12(double m12) {
		M12 = m12;
	}
	public Returns(double m1, double m3, double m6, double m12) {
		super();
		M1 = m1;
		M3 = m3;
		M6 = m6;
		M12 = m12;
	}

	
	
	
}
