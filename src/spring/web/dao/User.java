package spring.web.dao;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

public class User {
     
	@NotBlank(message="Te Pole nie mo�e by� puste")
	@Size(min=5, max=15, message="Nazwa u�ytkownika musi mie� pomi�dzy 5 a 15 liter")
	String username;
	
	@Email(message="Te Pole nie mo�e by� puste")
	String email;

	@NotBlank()
	@Pattern(regexp="^\\S+$")
	@Size(min=5, max=15, message="Has�o musi mie� pomi�dzy 5 a 15 liter")
	String password;

	private boolean enabled= false;
	private String authority;

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public User()
	{
		
	}
	
	public User(String username, String email, String password) {
		super();
		this.username = username;
		this.email = email;
		this.password = password;
		this.enabled= enabled; 
		this.authority= authority;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}
